"""
Helpful utilities that can be used
"""
import flask
import logging

def logger() -> logging.Logger:
	"""
	Shorthand to get type information of the built-in logger of flask
	"""
	return flask.current_app.logger
