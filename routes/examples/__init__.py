"""
The routes module contains standalone examples
"""
import flask
#	This is a child route, so we don't specify url_prefix like the previous one
router = flask.Blueprint("examples", __name__)

@router.get("/")
def page_home():
	return flask.render_template("./examples/home.html")

@router.get("/respond-string")
def response_string():
	return "This is a text being returned"

@router.get("/respond-html")
def response_html():
	return """
	<html>
		<body>
			<h1>Explanation</h1>
			<p>The returned string gets interpreted as HTML and rendered on the browser</p>
			<a href="#">This link does nothing</a>
		</body>
	</html>
	"""

@router.get("/respond-template")
def response_template():
	return flask.render_template("./examples/template-plain.html")

@router.get("/respond-template-substitution")
def response_template_substitution():
	"""
	You can subsitute things in your template by Key and Value
	in this case, 'content' is the key found in the template
	and the value is just my string (numbers also can)
	
	Exercise, Try other types and see the results
	"""
	return flask.render_template("./examples/template-plain.html",
				text_color = "red",
				content    = "<h5>This line is subsituted from python</h5>",
				condition  = False)

@router.get("/respond-template-loops")
def response_template_loops():
	"""
	The same as response_template_substitution but this time what if your value
	is a collection?

	This example shows how to iterate array and dictionary
	"""
	random_array    = [1, 2, 3, 4, 5, 6, 7, 8, 9 , 10]
	random_array_2d = [[1, 2, 3, 4, 5, 6, 7, 8, 9 , 10],[1, 2, 3, 4, 5, 6, 7, 8, 9 , 10],[1, 2, 3, 4, 5, 6, 7, 8, 9 , 10]]
	random_dict     = {
		"name": "GUY",
		"city": "FreeCity"
	}
	random_table   = {
		"numbers": [1, 2, 3 , 4],
		"text":    ["a", "b", "c", "d"]
	}
	
	return flask.render_template("./examples/template-loop.html", 
				list         = random_array,
				list2d       = random_array_2d,
				table        = random_dict,
				table_object = random_table)